@extends('layouts.master')
@section('title', 'Assignment')
@section('master')
@include('sweetalert::alert')
<div id="wrapper" class="wrapper bg-ash">
    <!-- Header Menu Area Start Here -->
    @include('layouts.navbar')
    <!-- Header Menu Area End Here -->
    <!-- Page Area Start Here -->
    <div class="dashboard-page-one">
        <!-- Sidebar Area Start Here -->
        @include('layouts.sidebar')
        <!-- Sidebar Area End Here -->
        <div class="dashboard-content-one">
            <!-- Breadcubs Area Start Here -->
            <div class="breadcrumbs-area">
                <h3>Phân công</h3>
                <ul>
                    <li>
                        <a href="{{ route('dashboard') }}">Trang chủ</a>
                    </li>
                    <li>Danh sách lịch phân công</li>
                </ul>
                <span>
                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        <i class="fas fa-check-circle"></i> {{ session('success') }}
                    </div>
                    @endif   

                    @if (session('warning'))
                    <div class="alert alert-warning" role="alert">
                        <i class="fas fa-exclamation-triangle"></i> {{ session('warning') }}
                    </div>
                    @endif      
                </span>
            </div>
            <!-- Breadcubs Area End Here -->
            <!-- Class Table Area Start Here -->
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3 col-lg-6 col-9 form-group pointcs">
                            <label>Lớp</label>
                            <select id="classes" name="classes_id" class="select2" required>
                                <option value="" disabled="" selected="">Hãy chọn lớp</option>
                                @foreach ($classes as $class)
                                    <option value="{{ $class->id }}">
                                        {{ $class->class_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-9 form-group pointcs" style="margin-left: 582px">
                         <a href="{{ route('assignment.view_insert') }}" class="btn btn-success btn-sm"
                         style="width: 85px;outline: none;margin-top: 33px;height: 40px;line-height: 29px;float: right;"><i class="fas fa-plus"
                         style="margin-right: 2px"></i>Thêm mới</a>
                     </div>
                 </div>
                 <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="assignment_tables" style="display: none">
                    <thead>
                        <tr>    
                            <th>Giáo viên</th>
                            <th>Môn học</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>   
            </div>
        </div>
    </div>
    <!-- Class Table Area End Here -->
    @include('layouts.footer')
</div>
</div>
<!-- Page Area End Here -->
</div>
@endsection


@push('scripts')
    <script>
        $('#classes').change(function() {
            $classes_id = $(this).val();
            if ($classes_id) {
                $.ajax({
                    url: '{{ route('assignment.get_assignment') }}',
                    type: 'get',
                    dataType: 'json',
                    data: {
                        classes_id: $classes_id,
                    },
                    success: function (data) {
                        $('#assignment_tables').attr("style", "border-top: 1px solid #ddd; margin-top: 30px;");
                        $('tbody').empty();
                        jQuery.each(data, function() {
                             $('tbody').append(`
                                <tr>
                                    <td>${ this.teacher_name }</td>
                                    <td>${ this.subject_name }</td>                           
                                </tr>
                            `);
                        });

                    }
                })
            }else{
                // $('tbody').empty();
                $('#assignment_tables').attr("style", "display: none");
            }
        });

        $(document).ready(function(){
            $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert").slideUp(500);
            });
        });
    </script>    
@endpush

