<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Assignment;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Department;
use App\Models\SubjectTeacher;
use App\Models\Teacher;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class AssignmentController extends Controller
{
    public function view_all(Request $request)
    {
        $classes = Classes::selectRaw("classes.id, concat(department.prefix,classes.classes_name,'K',course.course_name) as class_name, department_id, course_id")
        ->join('department', 'classes.department_id', 'department.id')
        ->join('course', 'classes.course_id', 'course.id')
        ->get();

        return view('assignment.view_all', [
            'classes' => $classes
        ]);
    }

    public function view_insert()
    {
        $Course = Course::all();
        $Department = Department::all();
        $teachers = Teacher::get(['teacher_name', 'id', 'images']);
        return view('assignment.view_insert', ['courses' => $Course, 'departments' => $Department, 'teachers' => $teachers]);
    }

    public function get_assignment(Request $request)
    {
        $classes_id = $request->get('classes_id');

        $assignments = Assignment::Where('classes_id', $classes_id)
        ->selectRaw("subject_name, teacher_name, classes_id, subject_id, teacher_id")
        ->leftjoin('classes', 'assignment.classes_id', 'classes.id')
        ->leftjoin('subject', 'assignment.subject_id', 'subject.id')
        ->leftjoin('teacher', 'assignment.teacher_id', 'teacher.id')
        ->get();

        return $assignments;
    }


    public function get_subject(Request $request)
    {
        return SubjectTeacher::with('subject')->where('teacher_id', $request->teacher_id)->get();
    }

    public function get_class(Request $request)
    {
        $assignment = Assignment::where(['teacher_id' => $request->teacher_id, 'subject_id' => $request->subject_id])
        ->get();
        $arr = [];
        foreach ($assignment as  $value) {
            $arr[] = $value->classes_id;
        }
        $classes = Classes::query()
        ->with(['department'=>function($query) use($request){
            $query->where('id',$request->department_id)->select('id','prefix');
        }])
        ->with(['course'=>function($query) use($request){
            $query->where('id',$request->course_id)->select('id','course_name');
        }])
        ->whereNotIn('id', $arr)->get();

        foreach ($classes as $key=> $value) {
            if($value->department!='' && $value->course!='')
            {
                $value->class_name=$value->department->prefix.$value->classes_name.
                'K'.$value->course->course_name;
                unset($value->department,$value->course,$value->classes_name);
            }
            else{
                unset($classes[$key]);
            }
        }
        return $classes;
    }

    public function process_insert(Request $request)
    {
        if($request->isMethod("post")){
            foreach ($request->classes as $value) {
                try {
                    Assignment::insert([
                        'classes_id' => $value,
                        'teacher_id' => $request->teacher_id,
                        'subject_id' => $request->subject_id,
                    ]);

                } catch (\Throwable $th) {
                }
            }
            return redirect()->route('assignment.view_all')->with('success', 'Thêm lịch phân công thành công');
        }else
        return redirect()->route('assignment.view_all')->with('warning', 'Phương thức truyền vào không đúng');

    }

    public function view_update($id)
    {
        $classes  = Classes::get();
        $assignments = DB::table('assignment')
            ->where('classes_id', $id)
            ->selectRaw("classes_name, teacher_name, subject_name, classes_id, teacher_id, subject_id
                ")
            ->join('teacher', 'assignment.teacher_id', '=', 'teacher.id')
            ->join('subject', 'assignment.subject_id', '=', 'subject.id')
            ->join('classes', 'assignment.classes_id', '=', 'classes.id')
            ->join('department', 'classes.department_id', '=', 'department.id')
            ->join('course', 'classes.course_id', '=', 'course.id')
            ->get();

         // dd($assignments);;

        return view('assignment.view_update', [
            'classes'   => $classes,
            'assignments' => $assignments
        ]);
    }
}
